package jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-18T15:36:05")
@StaticMetamodel(TimeChargePK.class)
public class TimeChargePK_ { 

    public static volatile SingularAttribute<TimeChargePK, Integer> timesheet;
    public static volatile SingularAttribute<TimeChargePK, String> wbs;
    public static volatile SingularAttribute<TimeChargePK, Integer> day;

}
package jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import jpa.entities.Employee;
import jpa.entities.LeaveDistributionPK;
import jpa.entities.Wbs;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-18T15:36:05")
@StaticMetamodel(LeaveDistribution.class)
public class LeaveDistribution_ { 

    public static volatile SingularAttribute<LeaveDistribution, LeaveDistributionPK> leaveDistributionPK;
    public static volatile SingularAttribute<LeaveDistribution, Wbs> wbs;
    public static volatile SingularAttribute<LeaveDistribution, Employee> employee;
    public static volatile SingularAttribute<LeaveDistribution, Double> percent;

}
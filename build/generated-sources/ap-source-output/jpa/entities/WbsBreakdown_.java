package jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import jpa.entities.Accounts;
import jpa.entities.Wbs;
import jpa.entities.WbsBreakdownPK;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-18T15:36:05")
@StaticMetamodel(WbsBreakdown.class)
public class WbsBreakdown_ { 

    public static volatile SingularAttribute<WbsBreakdown, Wbs> wbs;
    public static volatile SingularAttribute<WbsBreakdown, WbsBreakdownPK> wbsBreakdownPK;
    public static volatile SingularAttribute<WbsBreakdown, Accounts> accounts;
    public static volatile SingularAttribute<WbsBreakdown, Double> percent;

}
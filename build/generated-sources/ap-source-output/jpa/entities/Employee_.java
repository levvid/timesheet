package jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import jpa.entities.EmployeeType;
import jpa.entities.LeaveDistribution;
import jpa.entities.Timesheet;
import jpa.entities.Wbs;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-18T15:36:05")
@StaticMetamodel(Employee.class)
public class Employee_ { 

    public static volatile SingularAttribute<Employee, String> repliconName;
    public static volatile CollectionAttribute<Employee, Timesheet> timesheetCollection;
    public static volatile SingularAttribute<Employee, Integer> employeeId;
    public static volatile CollectionAttribute<Employee, Wbs> wbsCollection;
    public static volatile CollectionAttribute<Employee, LeaveDistribution> leaveDistributionCollection;
    public static volatile SingularAttribute<Employee, EmployeeType> type;

}
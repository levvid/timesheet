package jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import jpa.entities.Accounts;
import jpa.entities.FabAccountPK;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-18T15:36:05")
@StaticMetamodel(FabAccount.class)
public class FabAccount_ { 

    public static volatile SingularAttribute<FabAccount, Accounts> accounts1;
    public static volatile SingularAttribute<FabAccount, FabAccountPK> fabAccountPK;
    public static volatile SingularAttribute<FabAccount, Accounts> accounts;

}
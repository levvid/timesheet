package jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import jpa.entities.TimeChargePK;
import jpa.entities.Timesheet;
import jpa.entities.Wbs;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-18T15:36:05")
@StaticMetamodel(TimeCharge.class)
public class TimeCharge_ { 

    public static volatile SingularAttribute<TimeCharge, TimeChargePK> timeChargePK;
    public static volatile SingularAttribute<TimeCharge, String> description;
    public static volatile SingularAttribute<TimeCharge, Timesheet> timesheet1;
    public static volatile SingularAttribute<TimeCharge, Double> charged;
    public static volatile SingularAttribute<TimeCharge, Wbs> wbs1;

}
package jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import jpa.entities.Employee;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-18T15:36:05")
@StaticMetamodel(EmployeeType.class)
public class EmployeeType_ { 

    public static volatile CollectionAttribute<EmployeeType, Employee> employeeCollection;
    public static volatile SingularAttribute<EmployeeType, String> type;

}
package jpa.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-18T15:36:05")
@StaticMetamodel(LeaveDistributionPK.class)
public class LeaveDistributionPK_ { 

    public static volatile SingularAttribute<LeaveDistributionPK, Integer> employeeId;
    public static volatile SingularAttribute<LeaveDistributionPK, Date> payPeriodDate;
    public static volatile SingularAttribute<LeaveDistributionPK, String> wbsNumber;

}
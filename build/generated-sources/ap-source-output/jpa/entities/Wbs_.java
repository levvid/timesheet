package jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import jpa.entities.Employee;
import jpa.entities.LeaveDistribution;
import jpa.entities.TimeCharge;
import jpa.entities.WbsBreakdown;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-18T15:36:05")
@StaticMetamodel(Wbs.class)
public class Wbs_ { 

    public static volatile SingularAttribute<Wbs, Boolean> flagged;
    public static volatile CollectionAttribute<Wbs, WbsBreakdown> wbsBreakdownCollection;
    public static volatile SingularAttribute<Wbs, String> name;
    public static volatile CollectionAttribute<Wbs, TimeCharge> timeChargeCollection;
    public static volatile SingularAttribute<Wbs, Boolean> active;
    public static volatile CollectionAttribute<Wbs, LeaveDistribution> leaveDistributionCollection;
    public static volatile SingularAttribute<Wbs, String> wbsNumber;
    public static volatile SingularAttribute<Wbs, Employee> cam;

}
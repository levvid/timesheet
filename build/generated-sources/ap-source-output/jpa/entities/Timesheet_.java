package jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import jpa.entities.Employee;
import jpa.entities.TimeCharge;
import jpa.entities.TimesheetPK;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-18T15:36:05")
@StaticMetamodel(Timesheet.class)
public class Timesheet_ { 

    public static volatile SingularAttribute<Timesheet, TimesheetPK> timesheetPK;
    public static volatile SingularAttribute<Timesheet, Integer> month;
    public static volatile SingularAttribute<Timesheet, Integer> year;
    public static volatile SingularAttribute<Timesheet, Integer> firstday;
    public static volatile SingularAttribute<Timesheet, Integer> lastday;
    public static volatile CollectionAttribute<Timesheet, TimeCharge> timeChargeCollection;
    public static volatile SingularAttribute<Timesheet, Employee> employee1;
    public static volatile SingularAttribute<Timesheet, String> status;

}
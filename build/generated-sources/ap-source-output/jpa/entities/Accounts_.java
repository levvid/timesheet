package jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import jpa.entities.AccountsPK;
import jpa.entities.FabAccount;
import jpa.entities.WbsBreakdown;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-18T15:36:05")
@StaticMetamodel(Accounts.class)
public class Accounts_ { 

    public static volatile SingularAttribute<Accounts, Boolean> flagged;
    public static volatile CollectionAttribute<Accounts, FabAccount> fabAccountCollection;
    public static volatile SingularAttribute<Accounts, FabAccount> fabAccount;
    public static volatile CollectionAttribute<Accounts, WbsBreakdown> wbsBreakdownCollection;
    public static volatile SingularAttribute<Accounts, Boolean> active;
    public static volatile SingularAttribute<Accounts, AccountsPK> accountsPK;

}
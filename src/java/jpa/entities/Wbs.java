/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "wbs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Wbs.findAll", query = "SELECT w FROM Wbs w"),
    @NamedQuery(name = "Wbs.findByWbsNumber", query = "SELECT w FROM Wbs w WHERE w.wbsNumber = :wbsNumber"),
    @NamedQuery(name = "Wbs.findByActive", query = "SELECT w FROM Wbs w WHERE w.active = :active"),
    @NamedQuery(name = "Wbs.findByFlagged", query = "SELECT w FROM Wbs w WHERE w.flagged = :flagged"),
    @NamedQuery(name = "Wbs.findByName", query = "SELECT w FROM Wbs w WHERE w.name = :name")})
public class Wbs implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "wbs_number")
    private String wbsNumber;
    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active;
    @Basic(optional = false)
    @NotNull
    @Column(name = "flagged")
    private boolean flagged;
    @Size(max = 45)
    @Column(name = "name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "wbs1")
    private Collection<TimeCharge> timeChargeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "wbs")
    private Collection<WbsBreakdown> wbsBreakdownCollection;
    @JoinColumn(name = "cam", referencedColumnName = "employee_id")
    @ManyToOne
    private Employee cam;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "wbs")
    private Collection<LeaveDistribution> leaveDistributionCollection;

    public Wbs() {
    }

    public Wbs(String wbsNumber) {
        this.wbsNumber = wbsNumber;
    }

    public Wbs(String wbsNumber, boolean active, boolean flagged) {
        this.wbsNumber = wbsNumber;
        this.active = active;
        this.flagged = flagged;
    }

    public String getWbsNumber() {
        return wbsNumber;
    }

    public void setWbsNumber(String wbsNumber) {
        this.wbsNumber = wbsNumber;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean getFlagged() {
        return flagged;
    }

    public void setFlagged(boolean flagged) {
        this.flagged = flagged;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<TimeCharge> getTimeChargeCollection() {
        return timeChargeCollection;
    }

    public void setTimeChargeCollection(Collection<TimeCharge> timeChargeCollection) {
        this.timeChargeCollection = timeChargeCollection;
    }

    @XmlTransient
    public Collection<WbsBreakdown> getWbsBreakdownCollection() {
        return wbsBreakdownCollection;
    }

    public void setWbsBreakdownCollection(Collection<WbsBreakdown> wbsBreakdownCollection) {
        this.wbsBreakdownCollection = wbsBreakdownCollection;
    }

    public Employee getCam() {
        return cam;
    }

    public void setCam(Employee cam) {
        this.cam = cam;
    }

    @XmlTransient
    public Collection<LeaveDistribution> getLeaveDistributionCollection() {
        return leaveDistributionCollection;
    }

    public void setLeaveDistributionCollection(Collection<LeaveDistribution> leaveDistributionCollection) {
        this.leaveDistributionCollection = leaveDistributionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (wbsNumber != null ? wbsNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Wbs)) {
            return false;
        }
        Wbs other = (Wbs) object;
        if ((this.wbsNumber == null && other.wbsNumber != null) || (this.wbsNumber != null && !this.wbsNumber.equals(other.wbsNumber))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Wbs[ wbsNumber=" + wbsNumber + " ]";
    }
    
}

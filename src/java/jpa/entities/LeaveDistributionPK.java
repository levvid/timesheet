/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Gibson Levvid
 */
@Embeddable
public class LeaveDistributionPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "employee_id")
    private int employeeId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pay_period_date")
    @Temporal(TemporalType.DATE)
    private Date payPeriodDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "wbs_number")
    private String wbsNumber;

    public LeaveDistributionPK() {
    }

    public LeaveDistributionPK(int employeeId, Date payPeriodDate, String wbsNumber) {
        this.employeeId = employeeId;
        this.payPeriodDate = payPeriodDate;
        this.wbsNumber = wbsNumber;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public Date getPayPeriodDate() {
        return payPeriodDate;
    }

    public void setPayPeriodDate(Date payPeriodDate) {
        this.payPeriodDate = payPeriodDate;
    }

    public String getWbsNumber() {
        return wbsNumber;
    }

    public void setWbsNumber(String wbsNumber) {
        this.wbsNumber = wbsNumber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) employeeId;
        hash += (payPeriodDate != null ? payPeriodDate.hashCode() : 0);
        hash += (wbsNumber != null ? wbsNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LeaveDistributionPK)) {
            return false;
        }
        LeaveDistributionPK other = (LeaveDistributionPK) object;
        if (this.employeeId != other.employeeId) {
            return false;
        }
        if ((this.payPeriodDate == null && other.payPeriodDate != null) || (this.payPeriodDate != null && !this.payPeriodDate.equals(other.payPeriodDate))) {
            return false;
        }
        if ((this.wbsNumber == null && other.wbsNumber != null) || (this.wbsNumber != null && !this.wbsNumber.equals(other.wbsNumber))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.LeaveDistributionPK[ employeeId=" + employeeId + ", payPeriodDate=" + payPeriodDate + ", wbsNumber=" + wbsNumber + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "leave_distribution")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LeaveDistribution.findAll", query = "SELECT l FROM LeaveDistribution l"),
    @NamedQuery(name = "LeaveDistribution.findByEmployeeId", query = "SELECT l FROM LeaveDistribution l WHERE l.leaveDistributionPK.employeeId = :employeeId"),
    @NamedQuery(name = "LeaveDistribution.findByPercent", query = "SELECT l FROM LeaveDistribution l WHERE l.percent = :percent"),
    @NamedQuery(name = "LeaveDistribution.findByPayPeriodDate", query = "SELECT l FROM LeaveDistribution l WHERE l.leaveDistributionPK.payPeriodDate = :payPeriodDate"),
    @NamedQuery(name = "LeaveDistribution.findByWbsNumber", query = "SELECT l FROM LeaveDistribution l WHERE l.leaveDistributionPK.wbsNumber = :wbsNumber")})
public class LeaveDistribution implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LeaveDistributionPK leaveDistributionPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "percent")
    private double percent;
    @JoinColumn(name = "employee_id", referencedColumnName = "employee_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Employee employee;
    @JoinColumn(name = "wbs_number", referencedColumnName = "wbs_number", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Wbs wbs;

    public LeaveDistribution() {
    }

    public LeaveDistribution(LeaveDistributionPK leaveDistributionPK) {
        this.leaveDistributionPK = leaveDistributionPK;
    }

    public LeaveDistribution(LeaveDistributionPK leaveDistributionPK, double percent) {
        this.leaveDistributionPK = leaveDistributionPK;
        this.percent = percent;
    }

    public LeaveDistribution(int employeeId, Date payPeriodDate, String wbsNumber) {
        this.leaveDistributionPK = new LeaveDistributionPK(employeeId, payPeriodDate, wbsNumber);
    }

    public LeaveDistributionPK getLeaveDistributionPK() {
        return leaveDistributionPK;
    }

    public void setLeaveDistributionPK(LeaveDistributionPK leaveDistributionPK) {
        this.leaveDistributionPK = leaveDistributionPK;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Wbs getWbs() {
        return wbs;
    }

    public void setWbs(Wbs wbs) {
        this.wbs = wbs;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (leaveDistributionPK != null ? leaveDistributionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LeaveDistribution)) {
            return false;
        }
        LeaveDistribution other = (LeaveDistribution) object;
        if ((this.leaveDistributionPK == null && other.leaveDistributionPK != null) || (this.leaveDistributionPK != null && !this.leaveDistributionPK.equals(other.leaveDistributionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.LeaveDistribution[ leaveDistributionPK=" + leaveDistributionPK + " ]";
    }
    
}

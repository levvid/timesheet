/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Gibson Levvid
 */
@Embeddable
public class TimeChargePK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "timesheet")
    private int timesheet;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "wbs")
    private String wbs;
    @Basic(optional = false)
    @NotNull
    @Column(name = "day")
    private int day;

    public TimeChargePK() {
    }

    public TimeChargePK(int timesheet, String wbs, int day) {
        this.timesheet = timesheet;
        this.wbs = wbs;
        this.day = day;
    }

    public int getTimesheet() {
        return timesheet;
    }

    public void setTimesheet(int timesheet) {
        this.timesheet = timesheet;
    }

    public String getWbs() {
        return wbs;
    }

    public void setWbs(String wbs) {
        this.wbs = wbs;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) timesheet;
        hash += (wbs != null ? wbs.hashCode() : 0);
        hash += (int) day;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TimeChargePK)) {
            return false;
        }
        TimeChargePK other = (TimeChargePK) object;
        if (this.timesheet != other.timesheet) {
            return false;
        }
        if ((this.wbs == null && other.wbs != null) || (this.wbs != null && !this.wbs.equals(other.wbs))) {
            return false;
        }
        if (this.day != other.day) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.TimeChargePK[ timesheet=" + timesheet + ", wbs=" + wbs + ", day=" + day + " ]";
    }
    
}

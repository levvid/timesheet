/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "wbs_breakdown")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WbsBreakdown.findAll", query = "SELECT w FROM WbsBreakdown w"),
    @NamedQuery(name = "WbsBreakdown.findByWbsNumber", query = "SELECT w FROM WbsBreakdown w WHERE w.wbsBreakdownPK.wbsNumber = :wbsNumber"),
    @NamedQuery(name = "WbsBreakdown.findByAccount", query = "SELECT w FROM WbsBreakdown w WHERE w.wbsBreakdownPK.account = :account"),
    @NamedQuery(name = "WbsBreakdown.findBySubAccount", query = "SELECT w FROM WbsBreakdown w WHERE w.wbsBreakdownPK.subAccount = :subAccount"),
    @NamedQuery(name = "WbsBreakdown.findByPercent", query = "SELECT w FROM WbsBreakdown w WHERE w.percent = :percent")})
public class WbsBreakdown implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected WbsBreakdownPK wbsBreakdownPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "percent")
    private double percent;
    @JoinColumns({
        @JoinColumn(name = "account", referencedColumnName = "account", insertable = false, updatable = false),
        @JoinColumn(name = "sub_account", referencedColumnName = "sub_account", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Accounts accounts;
    @JoinColumn(name = "wbs_number", referencedColumnName = "wbs_number", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Wbs wbs;

    public WbsBreakdown() {
    }

    public WbsBreakdown(WbsBreakdownPK wbsBreakdownPK) {
        this.wbsBreakdownPK = wbsBreakdownPK;
    }

    public WbsBreakdown(WbsBreakdownPK wbsBreakdownPK, double percent) {
        this.wbsBreakdownPK = wbsBreakdownPK;
        this.percent = percent;
    }

    public WbsBreakdown(String wbsNumber, String account, String subAccount) {
        this.wbsBreakdownPK = new WbsBreakdownPK(wbsNumber, account, subAccount);
    }

    public WbsBreakdownPK getWbsBreakdownPK() {
        return wbsBreakdownPK;
    }

    public void setWbsBreakdownPK(WbsBreakdownPK wbsBreakdownPK) {
        this.wbsBreakdownPK = wbsBreakdownPK;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public Accounts getAccounts() {
        return accounts;
    }

    public void setAccounts(Accounts accounts) {
        this.accounts = accounts;
    }

    public Wbs getWbs() {
        return wbs;
    }

    public void setWbs(Wbs wbs) {
        this.wbs = wbs;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (wbsBreakdownPK != null ? wbsBreakdownPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WbsBreakdown)) {
            return false;
        }
        WbsBreakdown other = (WbsBreakdown) object;
        if ((this.wbsBreakdownPK == null && other.wbsBreakdownPK != null) || (this.wbsBreakdownPK != null && !this.wbsBreakdownPK.equals(other.wbsBreakdownPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.WbsBreakdown[ wbsBreakdownPK=" + wbsBreakdownPK + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "accounts")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Accounts.findAll", query = "SELECT a FROM Accounts a"),
    @NamedQuery(name = "Accounts.findByAccount", query = "SELECT a FROM Accounts a WHERE a.accountsPK.account = :account"),
    @NamedQuery(name = "Accounts.findBySubAccount", query = "SELECT a FROM Accounts a WHERE a.accountsPK.subAccount = :subAccount"),
    @NamedQuery(name = "Accounts.findByActive", query = "SELECT a FROM Accounts a WHERE a.active = :active"),
    @NamedQuery(name = "Accounts.findByFlagged", query = "SELECT a FROM Accounts a WHERE a.flagged = :flagged")})
public class Accounts implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AccountsPK accountsPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active;
    @Basic(optional = false)
    @NotNull
    @Column(name = "flagged")
    private boolean flagged;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "accounts")
    private Collection<WbsBreakdown> wbsBreakdownCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "accounts")
    private FabAccount fabAccount;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "accounts1")
    private Collection<FabAccount> fabAccountCollection;

    public Accounts() {
    }

    public Accounts(AccountsPK accountsPK) {
        this.accountsPK = accountsPK;
    }

    public Accounts(AccountsPK accountsPK, boolean active, boolean flagged) {
        this.accountsPK = accountsPK;
        this.active = active;
        this.flagged = flagged;
    }

    public Accounts(String account, String subAccount) {
        this.accountsPK = new AccountsPK(account, subAccount);
    }

    public AccountsPK getAccountsPK() {
        return accountsPK;
    }

    public void setAccountsPK(AccountsPK accountsPK) {
        this.accountsPK = accountsPK;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean getFlagged() {
        return flagged;
    }

    public void setFlagged(boolean flagged) {
        this.flagged = flagged;
    }

    @XmlTransient
    public Collection<WbsBreakdown> getWbsBreakdownCollection() {
        return wbsBreakdownCollection;
    }

    public void setWbsBreakdownCollection(Collection<WbsBreakdown> wbsBreakdownCollection) {
        this.wbsBreakdownCollection = wbsBreakdownCollection;
    }

    public FabAccount getFabAccount() {
        return fabAccount;
    }

    public void setFabAccount(FabAccount fabAccount) {
        this.fabAccount = fabAccount;
    }

    @XmlTransient
    public Collection<FabAccount> getFabAccountCollection() {
        return fabAccountCollection;
    }

    public void setFabAccountCollection(Collection<FabAccount> fabAccountCollection) {
        this.fabAccountCollection = fabAccountCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accountsPK != null ? accountsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Accounts)) {
            return false;
        }
        Accounts other = (Accounts) object;
        if ((this.accountsPK == null && other.accountsPK != null) || (this.accountsPK != null && !this.accountsPK.equals(other.accountsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Accounts[ accountsPK=" + accountsPK + " ]";
    }
    
}

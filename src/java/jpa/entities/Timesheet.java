/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "timesheet")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Timesheet.findAll", query = "SELECT t FROM Timesheet t"),
    @NamedQuery(name = "Timesheet.findByTimesheet", query = "SELECT t FROM Timesheet t WHERE t.timesheetPK.timesheet = :timesheet"),
    @NamedQuery(name = "Timesheet.findByEmployee", query = "SELECT t FROM Timesheet t WHERE t.timesheetPK.employee = :employee"),
    @NamedQuery(name = "Timesheet.findByStatus", query = "SELECT t FROM Timesheet t WHERE t.status = :status"),
    @NamedQuery(name = "Timesheet.findByYear", query = "SELECT t FROM Timesheet t WHERE t.year = :year"),
    @NamedQuery(name = "Timesheet.findByMonth", query = "SELECT t FROM Timesheet t WHERE t.month = :month"),
    @NamedQuery(name = "Timesheet.findByFirstday", query = "SELECT t FROM Timesheet t WHERE t.firstday = :firstday"),
    @NamedQuery(name = "Timesheet.findByLastday", query = "SELECT t FROM Timesheet t WHERE t.lastday = :lastday")})
public class Timesheet implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TimesheetPK timesheetPK;
    @Size(max = 45)
    @Column(name = "status")
    private String status;
    @Column(name = "year")
    private Integer year;
    @Column(name = "month")
    private Integer month;
    @Column(name = "firstday")
    private Integer firstday;
    @Column(name = "lastday")
    private Integer lastday;
    @JoinColumn(name = "employee", referencedColumnName = "employee_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Employee employee1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "timesheet1")
    private Collection<TimeCharge> timeChargeCollection;

    public Timesheet() {
    }

    public Timesheet(TimesheetPK timesheetPK) {
        this.timesheetPK = timesheetPK;
    }

    public Timesheet(int timesheet, int employee) {
        this.timesheetPK = new TimesheetPK(timesheet, employee);
    }

    public TimesheetPK getTimesheetPK() {
        return timesheetPK;
    }

    public void setTimesheetPK(TimesheetPK timesheetPK) {
        this.timesheetPK = timesheetPK;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getFirstday() {
        return firstday;
    }

    public void setFirstday(Integer firstday) {
        this.firstday = firstday;
    }

    public Integer getLastday() {
        return lastday;
    }

    public void setLastday(Integer lastday) {
        this.lastday = lastday;
    }

    public Employee getEmployee1() {
        return employee1;
    }

    public void setEmployee1(Employee employee1) {
        this.employee1 = employee1;
    }

    @XmlTransient
    public Collection<TimeCharge> getTimeChargeCollection() {
        return timeChargeCollection;
    }

    public void setTimeChargeCollection(Collection<TimeCharge> timeChargeCollection) {
        this.timeChargeCollection = timeChargeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (timesheetPK != null ? timesheetPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Timesheet)) {
            return false;
        }
        Timesheet other = (Timesheet) object;
        if ((this.timesheetPK == null && other.timesheetPK != null) || (this.timesheetPK != null && !this.timesheetPK.equals(other.timesheetPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Timesheet[ timesheetPK=" + timesheetPK + " ]";
    }
    
}

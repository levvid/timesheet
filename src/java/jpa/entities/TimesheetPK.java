/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Gibson Levvid
 */
@Embeddable
public class TimesheetPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "timesheet")
    private int timesheet;
    @Basic(optional = false)
    @NotNull
    @Column(name = "employee")
    private int employee;

    public TimesheetPK() {
    }

    public TimesheetPK(int timesheet, int employee) {
        this.timesheet = timesheet;
        this.employee = employee;
    }

    public int getTimesheet() {
        return timesheet;
    }

    public void setTimesheet(int timesheet) {
        this.timesheet = timesheet;
    }

    public int getEmployee() {
        return employee;
    }

    public void setEmployee(int employee) {
        this.employee = employee;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) timesheet;
        hash += (int) employee;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TimesheetPK)) {
            return false;
        }
        TimesheetPK other = (TimesheetPK) object;
        if (this.timesheet != other.timesheet) {
            return false;
        }
        if (this.employee != other.employee) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.TimesheetPK[ timesheet=" + timesheet + ", employee=" + employee + " ]";
    }
    
}

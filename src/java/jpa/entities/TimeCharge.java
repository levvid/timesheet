/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "time_charge")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TimeCharge.findAll", query = "SELECT t FROM TimeCharge t"),
    @NamedQuery(name = "TimeCharge.findByTimesheet", query = "SELECT t FROM TimeCharge t WHERE t.timeChargePK.timesheet = :timesheet"),
    @NamedQuery(name = "TimeCharge.findByWbs", query = "SELECT t FROM TimeCharge t WHERE t.timeChargePK.wbs = :wbs"),
    @NamedQuery(name = "TimeCharge.findByCharged", query = "SELECT t FROM TimeCharge t WHERE t.charged = :charged"),
    @NamedQuery(name = "TimeCharge.findByDescription", query = "SELECT t FROM TimeCharge t WHERE t.description = :description"),
    @NamedQuery(name = "TimeCharge.findByDay", query = "SELECT t FROM TimeCharge t WHERE t.timeChargePK.day = :day")})
public class TimeCharge implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TimeChargePK timeChargePK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "charged")
    private Double charged;
    @Size(max = 200)
    @Column(name = "description")
    private String description;
    @JoinColumns({
        @JoinColumn(name = "timesheet", referencedColumnName = "timesheet", insertable = false, updatable = false),
        @JoinColumn(name = "day", referencedColumnName = "day", insertable = false, updatable = false),})
    @ManyToOne(optional = false)
    private Timesheet timesheet1;
    @JoinColumn(name = "wbs", referencedColumnName = "wbs_number", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Wbs wbs1;

    public TimeCharge() {
    }

    public TimeCharge(TimeChargePK timeChargePK) {
        this.timeChargePK = timeChargePK;
    }

    public TimeCharge(int timesheet, String wbs, int day) {
        this.timeChargePK = new TimeChargePK(timesheet, wbs, day);
    }

    public TimeChargePK getTimeChargePK() {
        return timeChargePK;
    }

    public void setTimeChargePK(TimeChargePK timeChargePK) {
        this.timeChargePK = timeChargePK;
    }

    public Double getCharged() {
        return charged;
    }

    public void setCharged(Double charged) {
        this.charged = charged;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timesheet getTimesheet1() {
        return timesheet1;
    }

    public void setTimesheet1(Timesheet timesheet1) {
        this.timesheet1 = timesheet1;
    }

    public Wbs getWbs1() {
        return wbs1;
    }

    public void setWbs1(Wbs wbs1) {
        this.wbs1 = wbs1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (timeChargePK != null ? timeChargePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TimeCharge)) {
            return false;
        }
        TimeCharge other = (TimeCharge) object;
        if ((this.timeChargePK == null && other.timeChargePK != null) || (this.timeChargePK != null && !this.timeChargePK.equals(other.timeChargePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.TimeCharge[ timeChargePK=" + timeChargePK + " ]";
    }
    
}

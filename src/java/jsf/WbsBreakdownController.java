package jsf;

import jpa.entities.WbsBreakdown;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;
import jpa.session.WbsBreakdownFacade;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@Named("wbsBreakdownController")
@SessionScoped
public class WbsBreakdownController implements Serializable {

    private WbsBreakdown current;
    private DataModel items = null;
    @EJB
    private jpa.session.WbsBreakdownFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public WbsBreakdownController() {
    }

    public WbsBreakdown getSelected() {
        if (current == null) {
            current = new WbsBreakdown();
            current.setWbsBreakdownPK(new jpa.entities.WbsBreakdownPK());
            selectedItemIndex = -1;
        }
        return current;
    }

    private WbsBreakdownFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (WbsBreakdown) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new WbsBreakdown();
        current.setWbsBreakdownPK(new jpa.entities.WbsBreakdownPK());
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            current.getWbsBreakdownPK().setWbsNumber(current.getWbs().getWbsNumber());
            current.getWbsBreakdownPK().setAccount(current.getAccounts().getAccountsPK().getAccount());
            current.getWbsBreakdownPK().setSubAccount(current.getAccounts().getAccountsPK().getSubAccount());
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("WbsBreakdownCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (WbsBreakdown) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            current.getWbsBreakdownPK().setWbsNumber(current.getWbs().getWbsNumber());
            current.getWbsBreakdownPK().setAccount(current.getAccounts().getAccountsPK().getAccount());
            current.getWbsBreakdownPK().setSubAccount(current.getAccounts().getAccountsPK().getSubAccount());
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("WbsBreakdownUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (WbsBreakdown) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("WbsBreakdownDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public WbsBreakdown getWbsBreakdown(jpa.entities.WbsBreakdownPK id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = WbsBreakdown.class)
    public static class WbsBreakdownControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            WbsBreakdownController controller = (WbsBreakdownController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "wbsBreakdownController");
            return controller.getWbsBreakdown(getKey(value));
        }

        jpa.entities.WbsBreakdownPK getKey(String value) {
            jpa.entities.WbsBreakdownPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new jpa.entities.WbsBreakdownPK();
            key.setWbsNumber(values[0]);
            key.setAccount(values[1]);
            key.setSubAccount(values[2]);
            return key;
        }

        String getStringKey(jpa.entities.WbsBreakdownPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getWbsNumber());
            sb.append(SEPARATOR);
            sb.append(value.getAccount());
            sb.append(SEPARATOR);
            sb.append(value.getSubAccount());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof WbsBreakdown) {
                WbsBreakdown o = (WbsBreakdown) object;
                return getStringKey(o.getWbsBreakdownPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + WbsBreakdown.class.getName());
            }
        }

    }

}

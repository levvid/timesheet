package jsf;

import jpa.entities.Timesheet;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;
import jpa.session.TimesheetFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import jpa.entities.TimeCharge;
import org.primefaces.event.CellEditEvent;

@Named("timesheetController")
@SessionScoped
public class TimesheetController implements Serializable {

    private Timesheet current;
    private DataModel items = null;
    @EJB
    private jpa.session.TimesheetFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public TimesheetController() {
    }

    public Timesheet getSelected() {
        if (current == null) {
            current = new Timesheet();
            current.setTimesheetPK(new jpa.entities.TimesheetPK());
            selectedItemIndex = -1;
        }
        return current;
    }

    private TimesheetFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Timesheet) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Timesheet();
        current.setTimesheetPK(new jpa.entities.TimesheetPK());
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            current.getTimesheetPK().setEmployee(current.getEmployee1().getEmployeeId());
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("TimesheetCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Timesheet) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            current.getTimesheetPK().setEmployee(current.getEmployee1().getEmployeeId());
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("TimesheetUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Timesheet) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("TimesheetDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Timesheet getTimesheet(jpa.entities.TimesheetPK id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Timesheet.class)
    public static class TimesheetControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TimesheetController controller = (TimesheetController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "timesheetController");
            return controller.getTimesheet(getKey(value));
        }

        jpa.entities.TimesheetPK getKey(String value) {
            jpa.entities.TimesheetPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new jpa.entities.TimesheetPK();
            key.setTimesheet(Integer.parseInt(values[0]));
            key.setEmployee(Integer.parseInt(values[1]));
            return key;
        }

        String getStringKey(jpa.entities.TimesheetPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getTimesheet());
            sb.append(SEPARATOR);
            sb.append(value.getEmployee());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Timesheet) {
                Timesheet o = (Timesheet) object;
                return getStringKey(o.getTimesheetPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Timesheet.class.getName());
            }
        }

    }
    
    
    
    
    /*****Custom code******/
    private int timeSheet;
    private String employeeId = "test101", timeSheetNumber = "1", firstDay = "1", lastDay = "15";
    @PersistenceContext
    private EntityManager em;
    private List<TimeCharge> timeCharge;
    String day1;
    /**Retrieve a Timesheet with timesheeet = timeSheetNumber, employee = employeeId and 
     * firstday = firstDay.If the timesheet doesn't exist, calls createTimeSheet to create a new Timesheet.**/
    public void getTimeSheet(){
        String timeSheetQuery = "SELECT t FROM Timesheet t WHERE employee = '" + 
                employeeId + "' AND timesheet = '" + timeSheetNumber + 
                "' AND firstday = '" + firstDay + "'";
        
        try{
            current =  (Timesheet) em.createQuery(timeSheetQuery).getSingleResult();
        }
        //TimeSheet not found, create a new one
        catch(NoResultException e){
            e.printStackTrace();
            createTimeSheet();
        }
        
        
    }
    
    /**Creates a new Timesheet***/
    public Timesheet createTimeSheet(){
        current = new Timesheet();
        current.setTimesheetPK(new jpa.entities.TimesheetPK());
        
        return current;
    }
    
    
    /***Add a new TimeCharge belonging to TimeSheet tSheet*******/
    public void addTimeCharge(String tSheet, String wbs, String description, String day){
        
    }
    
    
    /**Cell Edit Listener
     * Listens for changes to cells and updates Timecharge table if the value on a cell changes*/
    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
        System.out.println("Got here");
        //get column header, then row number. Use get(i) and the header to identify and update the cell
        event.getColumn().getHeaderText();
        if(newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        
        
    }
    
    public List<TimeCharge> getTimeChargeItems(){
        System.out.println("These are the items before : ");
        String timeChargeQuery = "SELECT t FROM Timesheet t";// WHERE t.timeChargePK.timesheet = '" + 
                //timeSheetNumber + "'"; //AND t.day >= '" + firstDay + "' AND t.day <= '" + lastDay + "'";
        List<TimeCharge> timeChargeItems;
        timeChargeItems = em.createQuery(timeChargeQuery).getResultList();
        System.out.println("These are the items: "+ timeChargeItems.size());
        return timeChargeItems;
        
    }
    
    
    /*Return the number of hours charged to TimeCharge t where t.timesheet = timeSheetNumber, t.wbs = wbs, 
    * and t.day = day**/
    public String getDayCharge(String wbs, int day){
        String dayChargeQuery = "SELECT t FROM TimeCharge t"; // t WHERE t.timeChargePK.timesheet = '" + 
               // timeSheetNumber +   "' AND t.timeChargePK.day = '" + day + "'";
        //TimeCharge dayCharge = (TimeCharge) em.createQuery(dayChargeQuery).getSingleResult();
        //return dayCharge.getCharged().toString();
        return "23";
    }
    
    

}

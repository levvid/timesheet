package jsf;

import jpa.entities.LeaveDistribution;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;
import jpa.session.LeaveDistributionFacade;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@Named("leaveDistributionController")
@SessionScoped
public class LeaveDistributionController implements Serializable {

    private LeaveDistribution current;
    private DataModel items = null;
    @EJB
    private jpa.session.LeaveDistributionFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public LeaveDistributionController() {
    }

    public LeaveDistribution getSelected() {
        if (current == null) {
            current = new LeaveDistribution();
            current.setLeaveDistributionPK(new jpa.entities.LeaveDistributionPK());
            selectedItemIndex = -1;
        }
        return current;
    }

    private LeaveDistributionFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (LeaveDistribution) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new LeaveDistribution();
        current.setLeaveDistributionPK(new jpa.entities.LeaveDistributionPK());
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            current.getLeaveDistributionPK().setEmployeeId(current.getEmployee().getEmployeeId());
            current.getLeaveDistributionPK().setWbsNumber(current.getWbs().getWbsNumber());
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("LeaveDistributionCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (LeaveDistribution) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            current.getLeaveDistributionPK().setEmployeeId(current.getEmployee().getEmployeeId());
            current.getLeaveDistributionPK().setWbsNumber(current.getWbs().getWbsNumber());
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("LeaveDistributionUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (LeaveDistribution) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("LeaveDistributionDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public LeaveDistribution getLeaveDistribution(jpa.entities.LeaveDistributionPK id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = LeaveDistribution.class)
    public static class LeaveDistributionControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            LeaveDistributionController controller = (LeaveDistributionController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "leaveDistributionController");
            return controller.getLeaveDistribution(getKey(value));
        }

        jpa.entities.LeaveDistributionPK getKey(String value) {
            jpa.entities.LeaveDistributionPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new jpa.entities.LeaveDistributionPK();
            key.setEmployeeId(Integer.parseInt(values[0]));
            key.setPayPeriodDate(java.sql.Date.valueOf(values[1]));
            key.setWbsNumber(values[2]);
            return key;
        }

        String getStringKey(jpa.entities.LeaveDistributionPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getEmployeeId());
            sb.append(SEPARATOR);
            sb.append(value.getPayPeriodDate());
            sb.append(SEPARATOR);
            sb.append(value.getWbsNumber());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof LeaveDistribution) {
                LeaveDistribution o = (LeaveDistribution) object;
                return getStringKey(o.getLeaveDistributionPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + LeaveDistribution.class.getName());
            }
        }

    }

}
